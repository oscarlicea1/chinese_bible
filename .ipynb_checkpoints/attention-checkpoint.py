import torch.nn as nn
import torch.nn.functional as F

class AttnLSTM(nn.Module):
    
    def __init__(self, input_dim, embed_dim, hidden_dim, num_layers, dropout, seq_length):
        super().__init__() #inherent the nn.Module class
        
        self.embed = nn.Embedding(input_dim, embed_dim)
        self.attn = nn.Linear(hidden_dim * 2, seq_length)
        self.attn_combine = nn.Linear(hidden_dim * 2, hidden_dim)
        self.lstm = nn.LSTM(embed_dim, hidden_dim, num_layers=num_layers, dropout=dropout)
        self.fc = nn.Linear(hidden_dim, input_dim) #Since input space equals output space
        
    def forward(self, sentence, hidden):
        
        embed = self.embed(sentence.t())
        
        attn_weights = F.softmax(self.attn(embed[0], hidden[0], 1), dim = 1)
        attn_applied = torch.bmm(attn_weights.unsqueeze(0),
                                embed[0])
        
        output = F.relu(attn_applied)
        
        lstm_out, (h_n, c_n) = self.lstm(output)
        output = self.fc(lstm_out[-1])
        
        return output, h_n, attn_weights